﻿namespace Actividad4
{
    partial class FormAgregar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonAgregar = new System.Windows.Forms.Button();
            this.labelNoNodos = new System.Windows.Forms.Label();
            this.textBoxNoNodos = new System.Windows.Forms.TextBox();
            this.comboBoxTipoConfiguracion = new System.Windows.Forms.ComboBox();
            this.labelConfiguracion = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // buttonAgregar
            // 
            this.buttonAgregar.Location = new System.Drawing.Point(22, 95);
            this.buttonAgregar.Name = "buttonAgregar";
            this.buttonAgregar.Size = new System.Drawing.Size(224, 23);
            this.buttonAgregar.TabIndex = 3;
            this.buttonAgregar.Text = "Agregar";
            this.buttonAgregar.UseVisualStyleBackColor = true;
            this.buttonAgregar.Click += new System.EventHandler(this.ButtonAgregar_Click);
            // 
            // labelNoNodos
            // 
            this.labelNoNodos.AutoSize = true;
            this.labelNoNodos.Location = new System.Drawing.Point(19, 33);
            this.labelNoNodos.Name = "labelNoNodos";
            this.labelNoNodos.Size = new System.Drawing.Size(91, 13);
            this.labelNoNodos.TabIndex = 0;
            this.labelNoNodos.Text = "Número de nodos";
            // 
            // textBoxNoNodos
            // 
            this.textBoxNoNodos.Location = new System.Drawing.Point(116, 30);
            this.textBoxNoNodos.Name = "textBoxNoNodos";
            this.textBoxNoNodos.Size = new System.Drawing.Size(130, 20);
            this.textBoxNoNodos.TabIndex = 1;
            // 
            // comboBoxTipoConfiguracion
            // 
            this.comboBoxTipoConfiguracion.FormattingEnabled = true;
            this.comboBoxTipoConfiguracion.Items.AddRange(new object[] {
            "First fit",
            "Best fit",
            "Best fit extended"});
            this.comboBoxTipoConfiguracion.Location = new System.Drawing.Point(135, 56);
            this.comboBoxTipoConfiguracion.Name = "comboBoxTipoConfiguracion";
            this.comboBoxTipoConfiguracion.Size = new System.Drawing.Size(111, 21);
            this.comboBoxTipoConfiguracion.TabIndex = 2;
            this.comboBoxTipoConfiguracion.SelectedItem = null;
            this.comboBoxTipoConfiguracion.SelectedText = "--seleccionar--";
            // 
            // labelConfiguracion
            // 
            this.labelConfiguracion.AutoSize = true;
            this.labelConfiguracion.Location = new System.Drawing.Point(19, 59);
            this.labelConfiguracion.Name = "labelConfiguracion";
            this.labelConfiguracion.Size = new System.Drawing.Size(110, 13);
            this.labelConfiguracion.TabIndex = 4;
            this.labelConfiguracion.Text = "Tipo de configuración";
            // 
            // FormAgregar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(260, 140);
            this.Controls.Add(this.labelConfiguracion);
            this.Controls.Add(this.comboBoxTipoConfiguracion);
            this.Controls.Add(this.textBoxNoNodos);
            this.Controls.Add(this.labelNoNodos);
            this.Controls.Add(this.buttonAgregar);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "FormAgregar";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Agregar programa";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonAgregar;
        private System.Windows.Forms.Label labelNoNodos;
        private System.Windows.Forms.TextBox textBoxNoNodos;
        private System.Windows.Forms.ComboBox comboBoxTipoConfiguracion;
        private System.Windows.Forms.Label labelConfiguracion;
    }
}