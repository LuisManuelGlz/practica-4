﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Actividad4
{
    public partial class FormAgregar : Form
    {
        private FormStart formStart;

        public FormAgregar(FormStart formStart)
        {
            InitializeComponent();
            this.formStart = formStart;
            buttonAgregar.Text = String.Format("Agregar programa #{0}", formStart.contador);
        }

        private void ButtonAgregar_Click(object sender, EventArgs e)
        {
            int index = 0;
            string noNodos = textBoxNoNodos.Text;
            string tipoConfiguracion = comboBoxTipoConfiguracion.GetItemText(comboBoxTipoConfiguracion.SelectedItem);

            // si todos los campos están bien
            if (noNodos.Length > 0 && int.Parse(noNodos) <= 100 && !String.IsNullOrEmpty(tipoConfiguracion))
            {
                if (tipoConfiguracion == "First fit")
                {
                    index = Array.IndexOf(formStart.nodos, 0);

                    if (formStart.nodos[index] == 0)
                    {
                        for (int i = 0; i < int.Parse(noNodos); i++)
                        {
                            formStart.nodos[index+i] = formStart.contador;
                        }
                    }
                    else
                    {
                        MessageBox.Show("Ya no hay espacio", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }
                else if (tipoConfiguracion == "Best fit")
                {
                    index = ChecaEspeacio(formStart.nodos, int.Parse(noNodos), tipoConfiguracion);

                    if (index != -1)
                    {
                        for (int i = 0; i < int.Parse(noNodos); i++)
                        {
                            formStart.nodos[index - i] = formStart.contador;
                        }
                    }
                    else
                    {
                        MessageBox.Show("Ya no hay espacio", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }
                else if (tipoConfiguracion == "Best fit extended")
                {
                    index = ChecaEspeacio(formStart.nodos, int.Parse(noNodos), tipoConfiguracion);

                    if (index != -1)
                    {
                        int i = 0;

                        while (true)
                        {
                            if (formStart.nodos[index-i] == 0)
                            {
                                formStart.nodos[index-i] = formStart.contador;
                                i++;
                            }
                            else
                            {
                                break;
                            }
                        }
                    }
                    else
                    {
                        MessageBox.Show("Ya no hay espacio", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }
                formStart.textBoxNodos.Text = string.Join("  |  ", formStart.nodos);
                formStart.contador += 1;
                this.Close();
            }
            else
            {
                if (noNodos.Length == 0)
                {
                    MessageBox.Show("El campo \"Número de nodos\" no puede quedar vacío", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                //if (int.Parse(noNodos) > 100)
                //{
                //    MessageBox.Show("El campo \"Número de nodos\" no puede ser mayor a 100", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                //}
                if (String.IsNullOrEmpty(tipoConfiguracion))
                {
                    MessageBox.Show("El campo \"Tipo de configuración\" no puede quedar vacío", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }

        

        private int ChecaEspeacio(int[] nodos, int noNodos, string config)
        {
            int espacios = 0;

            if (config == "Best fit")
            {
                for (int index=0; index<nodos.Length; index++)
                {
                    if (nodos[index] == 0)
                    {
                        espacios += 1;

                        if (noNodos == espacios)
                        {
                            return index;
                        }
                    }
                    else
                    {
                        espacios = 0;
                    }
                }
                return -1;
            }
            else if (config == "Best fit extended")
            {
                int index;

                for (index = 0; index < nodos.Length-1; index++)
                {
                    if (nodos[index] == 0)
                    {
                        espacios += 1;
                        // falata poner algo aquí
                    }
                    else
                    {
                        espacios = 0;
                    }
                }
                if (noNodos <= espacios)
                {
                    return index;
                }
                return -1;
            }
            return -1;
        }
    }
}
