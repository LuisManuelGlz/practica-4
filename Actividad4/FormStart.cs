﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Actividad4
{
    public partial class FormStart : Form
    {
        public int contador;
        public int[] nodos;

        public FormStart()
        {
            InitializeComponent();
            this.contador = 1;
            nodos = new int[100];
        }

        private void ButtonAgregar_Click(object sender, EventArgs e)
        {
            FormAgregar formAgregar = new FormAgregar(this);
            formAgregar.Show();
        }

        private void ButtonEliminar_Click(object sender, EventArgs e)
        {
            FormEliminar formEliminar = new FormEliminar(this);
            formEliminar.Show();
        }

        private void ButtonSalir_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
