﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Actividad4
{
    public partial class FormEliminar : Form
    {
        private FormStart formStart;

        public FormEliminar(FormStart formStart)
        {
            InitializeComponent();
            this.formStart = formStart;
        }

        private void ButtonEliminar_Click(object sender, EventArgs e)
        {
            string noPrograma = textBoxNoPrograma.Text;

            if (!String.IsNullOrEmpty(noPrograma))
            {
                for (int i = 0; i < formStart.nodos.Length; i++)
                {
                    if (formStart.nodos[i] == int.Parse(noPrograma))
                    {
                        formStart.nodos[i] = 0;
                    }
                }
                formStart.textBoxNodos.Text = string.Join("  |  ", formStart.nodos);
                this.Close();
            }
            else
            {
                MessageBox.Show("El campo \"Número d programa\" no puede quedar vacío", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
    }
}
